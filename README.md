TwFormats Blender Add-on
===

Import your favorite Teeworlds and DDNet maps into Blender!
Use the power and versatility of Blender to create all sorts of amazing animations.

This Blender add-on is built on top of my `twmap` [Python module](https://gitlab.com/Patiga/twmap-py), which itself it build on top of my `twmap` [Rust library](https://crates.io/crates/twmap)!

Installations
---

- Download the latest `twformats-<version>.zip` from the [Releases](https://gitlab.com/ddnet-rs/twblender/-/releases) page
- Simply go to the add-ons menu in Blender (`Edit > Preferences > Add-ons`), click `Install` and navigate to the .zip file

Usage
---

`File > Import > Teeworlds Map (.map)`

Things to note
---

- Group clipping and map sounds are not yet implemented
- Each map has a camera dedicated to it. Use its view while moving it around to get the 'correct view' of the map (parallax-wise)
- The length of the importing process scales with the size of the map
- The map might look very dark when you import it.
Use a light source of the type `Sun` with a strength of 2 to 3
- During the importing the add-on will set the `Render Properties > Color Management > View Transform` to `Standard`, which makes the colors look right in the maps

Example Animations
---

[Water Simulation](https://youtu.be/88xtJ_jLVM8)

[Experimental Map Intro](https://youtu.be/gVWF1N1e-xo)

[Miner Envelopes](https://youtu.be/bgEpIIG1k4c)
