import bpy

from . import map_import

def menu_func(self, context):
    self.layout.operator(map_import.ImportTwMap.bl_idname)


def register():
    # Make sure twmap is installed and importable
    bpy.utils.register_class(map_import.ImportTwMap)
    bpy.types.TOPBAR_MT_file_import.append(menu_func)


def unregister():
    bpy.types.TOPBAR_MT_file_import.remove(menu_func)
    bpy.utils.unregister_class(map_import.ImportTwMap)


if __name__ == "__main__":
    register()
