import bpy
import math


def convert_envelope(envelope, index, init_keyframes, convert_fn, kps):
    env_points = envelope.points
    if len(env_points) == 0:
        return True, convert_fn((0, 0, 0, 0))
    elif len(env_points) == 1:
        return True, convert_fn(env_points[0].content)
    else:
        action_name = f"{envelope.kind()} Envelope ({index})"
        if envelope.name:
            action_name = f"{envelope.name} {action_name}"
        action = bpy.data.actions.new(action_name)
        keyframe_sets = init_keyframes(action)
        if env_points[0].time != 0:
            for keyframes, value in zip(keyframe_sets, convert_fn(env_points[-1].content)):
                keyframe = keyframes.insert(0, value)
                keyframe.interpolation = "CONSTANT"
        for point in env_points:
            for keyframes, value in zip(keyframe_sets, convert_fn(point.content)):
                keyframe = keyframes.insert(point.time * kps / 1000, value)
                curve = point.curve
                if curve == "Step":
                    interpolation = "CONSTANT"
                elif curve == "Linear":
                    interpolation = "LINEAR"
                elif curve == "Slow":
                    interpolation = "CUBIC"
                elif curve == "Fast":
                    interpolation = "CUBIC"
                    keyframe.easing = "EASE_OUT"
                elif curve == "Smooth":
                    interpolation = "BEZIER"
                elif curve == "Bezier":
                    interpolation = "BEZIER"
                    # TODO: use bezier values for better 0.7 compatibility
                keyframe.interpolation = interpolation
        return False, action


def convert_position(position):
    return position[0], -position[1], math.radians(-position[2])


def init_position_keyframes(action):
    x_keyframes = action.fcurves.new("delta_location", index=0).keyframe_points
    y_keyframes = action.fcurves.new("delta_location", index=1).keyframe_points
    rotation_keyframes = action.fcurves.new("delta_rotation_euler", index=2).keyframe_points
    return x_keyframes, y_keyframes, rotation_keyframes


def convert_color(color):
    return [color**2 for color in color[:3]] + [int(color[3] * 1024)]


def init_color_keyframes(action):
    r_keyframes = action.fcurves.new("color", index=0).keyframe_points
    g_keyframes = action.fcurves.new("color", index=1).keyframe_points
    b_keyframes = action.fcurves.new("color", index=2).keyframe_points
    a_keyframes = action.fcurves.new("pass_index").keyframe_points
    return r_keyframes, g_keyframes, b_keyframes, a_keyframes


class Envelopes:
    def __init__(self, map_envelopes):
        scene = bpy.context.scene
        render = scene.render
        self.kps = render.fps / render.fps_base * render.frame_map_old / render.frame_map_new

        actions = []
        is_constant = []

        for i, envelope in enumerate(map_envelopes):
            env_kind = envelope.kind()
            if env_kind == "Position":
                init_keyframes = init_position_keyframes
                convert_fn = convert_position
            elif env_kind == "Color":
                init_keyframes = init_color_keyframes
                convert_fn = convert_color
            else:  # TODO: Sound envelopes
                is_constant.append(None)
                actions.append(None)
                continue
            constant, action = convert_envelope(envelope, i, init_keyframes, convert_fn, self.kps)
            is_constant.append(constant)
            actions.append(action)

        self.actions = actions
        self.is_constant = is_constant

    def apply_action(self, obj, env_index, env_offset, identifier):
        nla_track = obj.animation_data_create().nla_tracks.new()
        nla_track.name = f"TwMap {identifier} Envelope"
        start = env_offset / 1000 * self.kps
        action = self.actions[env_index]
        if start > 0:
            action_length = action.frame_range[1] - action.frame_range[0]
            start = start % action_length
        nla_strip = nla_track.strips.new(f"{identifier} Env", round(-start), action)
        nla_strip.extrapolation = "HOLD_FORWARD"
        nla_strip.repeat = 1000
