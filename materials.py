import bpy
import tempfile
import os


def new_material(material_name):
    material = bpy.data.materials.new(name=material_name)
    material.shadow_method = "NONE"
    material.blend_method = "BLEND"
    material.use_nodes = True
    bsdf = material.node_tree.nodes["Principled BSDF"]
    bsdf.inputs["Specular IOR Level"].default_value = 0.0
    return material, bsdf


def new_node_group(node_tree, group_node_tree):
    group_node = node_tree.nodes.new('ShaderNodeGroup')
    group_node.node_tree = group_node_tree
    return group_node


def new_image_node(node_tree, image):
    image_node = node_tree.nodes.new("ShaderNodeTexImage")
    image_node.image = image
    image_node.interpolation = "Closest"
    return image_node


def new_rgb_node(node_tree, color):
    color_node = node_tree.nodes.new("ShaderNodeRGB")
    output = color_node.outputs["Color"]
    output.default_value = color
    return output


def new_value_node(node_tree, value):
    value_node = node_tree.nodes.new("ShaderNodeValue")
    output = value_node.outputs["Value"]
    output.default_value = value
    return output


def new_color_mix_node(node_tree, clamp=False):
    mix_node = node_tree.nodes.new("ShaderNodeMixRGB")
    mix_node.blend_type = "MULTIPLY"
    mix_node.inputs["Fac"].default_value = 1.0
    mix_node.use_clamp = clamp
    return mix_node


def new_vertex_color_node(node_tree):
    vertex_color_node = node_tree.nodes.new("ShaderNodeVertexColor")
    return vertex_color_node


def new_object_info_node(node_tree):
    value_mix_node = node_tree.nodes.new("ShaderNodeObjectInfo")
    return value_mix_node


def new_value_divide_1024_node(node_tree):
    math_node = node_tree.nodes.new("ShaderNodeMath")
    math_node.operation = "DIVIDE"
    math_node.inputs[1].default_value = 1024.0
    return math_node


def new_value_mix_node(node_tree, clamp=False):
    math_node = node_tree.nodes.new("ShaderNodeMath")
    math_node.operation = "MULTIPLY"
    math_node.use_clamp = clamp
    return math_node


# This function creates the reusable shader material node group for quads.
def quad_node_group():
    group = bpy.data.node_groups.new(name="TwMap Quads Group", type='ShaderNodeTree')
    group_in = group.nodes.new("NodeGroupInput").outputs
    group_out = group.nodes.new("NodeGroupOutput").inputs
    links = group.links

    group.interface.new_socket("Color", in_out="INPUT", socket_type="NodeSocketColor")
    group.interface.new_socket("Alpha", in_out="INPUT", socket_type="NodeSocketFloat")
    group.interface.new_socket("Color", in_out="OUTPUT", socket_type="NodeSocketColor")
    group.interface.new_socket("Alpha", in_out="OUTPUT", socket_type="NodeSocketFloat")

    vertex_color_node = new_vertex_color_node(group)

    first_color_mix_node = new_color_mix_node(group)
    links.new(first_color_mix_node.inputs["Color1"], group_in["Color"])
    links.new(first_color_mix_node.inputs["Color2"], vertex_color_node.outputs["Color"])

    first_alpha_mix_node = new_value_mix_node(group)
    links.new(first_alpha_mix_node.inputs[0], group_in["Alpha"])
    links.new(first_alpha_mix_node.inputs[1], vertex_color_node.outputs["Alpha"])

    object_info_node = new_object_info_node(group)
    divide_node = new_value_divide_1024_node(group)
    links.new(divide_node.inputs[0], object_info_node.outputs["Object Index"])

    second_color_mix_node = new_color_mix_node(group, clamp=True)
    links.new(second_color_mix_node.inputs["Color1"], first_color_mix_node.outputs["Color"])
    links.new(second_color_mix_node.inputs["Color2"], object_info_node.outputs["Color"])

    second_value_mix_node = new_value_mix_node(group, clamp=True)
    links.new(second_value_mix_node.inputs[0], first_alpha_mix_node.outputs["Value"])
    links.new(second_value_mix_node.inputs[1], divide_node.outputs["Value"])

    links.new(group_out["Color"], second_color_mix_node.outputs["Color"])
    links.new(group_out["Alpha"], second_value_mix_node.outputs["Value"])

    return group


# This function creates the reusable shader material node group for tilemap meshes.
def tilemap_mesh_node_group():
    group = bpy.data.node_groups.new(name="TwMap Quads Group", type="ShaderNodeTree")
    group_in = group.nodes.new("NodeGroupInput").outputs
    group_out = group.nodes.new("NodeGroupOutput").inputs
    links = group.links

    group.interface.new_socket("Color", in_out="INPUT", socket_type="NodeSocketColor")
    group.interface.new_socket("Alpha", in_out="INPUT", socket_type="NodeSocketFloat")
    group.interface.new_socket("Color", in_out="OUTPUT", socket_type="NodeSocketColor")
    group.interface.new_socket("Alpha", in_out="OUTPUT", socket_type="NodeSocketFloat")

    object_info_node = new_object_info_node(group)
    divide_node = new_value_divide_1024_node(group)
    links.new(divide_node.inputs[0], object_info_node.outputs["Object Index"])

    second_color_mix_node = new_color_mix_node(group, clamp=True)
    links.new(second_color_mix_node.inputs["Color1"], group_in["Color"])
    links.new(second_color_mix_node.inputs["Color2"], object_info_node.outputs["Color"])

    second_value_mix_node = new_value_mix_node(group, clamp=True)
    links.new(second_value_mix_node.inputs[0], group_in["Alpha"])
    links.new(second_value_mix_node.inputs[1], divide_node.outputs["Value"])

    links.new(group_out["Color"], second_color_mix_node.outputs["Color"])
    links.new(group_out["Alpha"], second_value_mix_node.outputs["Value"])

    return group


class Materials:
    def __init__(self, map_images):
        images = []
        with tempfile.TemporaryDirectory() as tmp_dir:
            for i, image in enumerate(map_images):
                image_path = os.path.join(tmp_dir, f"{i}_{image.name}.png")
                image.save(image_path)
                b_image = bpy.data.images.load(filepath=image_path)
                b_image.pack()
                b_image.filepath = ""
                images.append(b_image)
        self.quad_node_tree = quad_node_group()
        self.tilemap_mesh_node_tree = tilemap_mesh_node_group()
        self.images = images
        self.array_images = {}
        self.quad_materials = {}
        self.tilemap_materials = {}

    def array_texture(self, image_index):
        if image_index in self.array_images:
            return self.array_images[image_index]
        with tempfile.TemporaryDirectory() as tmp_dir:
            image = self.map_images[image_index]
            data = image.data
            tile_size = image.width() // 16
            for y in range(16):
                for x in range(16):
                    tile_data = data[y*tile_size:(y+1)*tile_size, x*tile_size:(x+1)*tile_size]
                    idx = y * 16 + x
                    name = f"{image_index}_{image.name}_{1001 + idx}.png"
                    Image.fromarray(tile_data).save(os.path.join(tmp_dir, name))
            full_name = f"{image_index}_{image.name}_<UDIM>.png"
            image_path = os.path.join(tmp_dir, full_name)
            b_image = bpy.data.images.new(full_name, 1, 1, tiled=True)
            b_image.filepath = image_path
            b_image.reload()
            b_image.pack()
            b_image.filepath = ""
            self.array_images[image_index] = b_image
            return b_image

    def quad_material(self, image_index):
        if image_index in self.quad_materials:
            return self.quad_materials[image_index]
        if image_index is None:
            material_name = "Quad untextured"
        else:
            material_name = f"Quad {self.images[image_index].name}"
        material, bsdf = new_material(material_name)
        links = material.node_tree.links

        quad_group = new_node_group(material.node_tree, self.quad_node_tree)
        links.new(bsdf.inputs["Base Color"], quad_group.outputs["Color"])
        links.new(bsdf.inputs["Alpha"], quad_group.outputs["Alpha"])

        if image_index is not None:
            image_node = new_image_node(material.node_tree, self.images[image_index])
            links.new(image_node.outputs["Color"], quad_group.inputs["Color"])
            links.new(image_node.outputs["Alpha"], quad_group.inputs["Alpha"])
        else:
            rgb_val = new_rgb_node(material.node_tree, [1.] * 4)
            links.new(rgb_val, quad_group.inputs["Color"])
            alpha_val = new_value_node(material.node_tree, 1.)
            links.new(alpha_val, quad_group.inputs["Alpha"])

        self.quad_materials[image_index] = material
        return self.quad_materials[image_index]


    def tiles_layer_material(self, color, image_index):
        if (image_index, color) in self.tilemap_materials:
            return self.tilemap_materials[(image_index, color)]

        if image_index is None:
            material_name = f"Tiles untextured {color}"
        else:
            material_name = f"Tiles {self.images[image_index].name} {color}"
        material, bsdf = new_material(material_name)
        links = material.node_tree.links

        tilemap_group = new_node_group(material.node_tree, self.tilemap_mesh_node_tree)
        links.new(bsdf.inputs["Base Color"], tilemap_group.outputs["Color"])
        links.new(bsdf.inputs["Alpha"], tilemap_group.outputs["Alpha"])

        rgb_val = new_rgb_node(material.node_tree, [v / 255 for v in color])
        alpha_val = new_value_node(material.node_tree, color[3] / 255)

        if image_index is not None:
            image_node = new_image_node(material.node_tree, self.images[image_index])
            color_mix_node = new_color_mix_node(material.node_tree)
            links.new(color_mix_node.inputs["Color1"], image_node.outputs["Color"])
            links.new(color_mix_node.inputs["Color2"], rgb_val) 

            alpha_mix_node = new_value_mix_node(material.node_tree)
            links.new(alpha_mix_node.inputs[0], image_node.outputs["Alpha"])
            links.new(alpha_mix_node.inputs[1], alpha_val)

            links.new(tilemap_group.inputs["Color"], color_mix_node.outputs["Color"])
            links.new(tilemap_group.inputs["Alpha"], alpha_mix_node.outputs["Value"])
        else:
            links.new(tilemap_group.inputs["Color"], rgb_val)
            links.new(tilemap_group.inputs["Alpha"], alpha_val)

        self.tilemap_materials[(image_index, color)] = material
        return self.tilemap_materials[(image_index, color)]
