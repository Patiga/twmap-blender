import bpy
import os

from bpy_extras.io_utils import ImportHelper
from bpy.types import Operator
from bpy.props import StringProperty


class ImportTwMap(Operator, ImportHelper):
    """Import a Teeworlds/DDNet map"""  # tooltip
    bl_idname = "import_scene.twmap"  # unique identifier
    bl_label = "Teeworlds Map (.map)"  # button label
    bl_options = {"REGISTER", "UNDO"}  # enable undo

    filename_ext = ".map"

    filter_glob: StringProperty(
        default="*.map",
        options={"HIDDEN"}
    )

    def execute(self, context):
        import twmap
        from .materials import Materials
        from .envelopes import Envelopes

        map = twmap.Map(self.filepath)
        bpy.context.scene.view_settings.view_transform = "Standard"

        # embed external images
        script_file = os.path.realpath(__file__)
        directory = os.path.dirname(script_file)
        if map.version() == "DDNet06":
            mapres_dir = os.path.join(directory, "mapres_06")
        else:
            mapres_dir = os.path.join(directory, "mapres_07")
        map.embed_images(mapres_dir)
        materials = Materials(map.images)
        envelopes = Envelopes(map.envelopes)
        import_layers(map.groups, materials, self.filepath, envelopes)
        return {"FINISHED"}


def import_layers(map_groups, materials, map_path, envelopes):
    map_name = os.path.splitext(os.path.basename(map_path))[0]
    map_empty = bpy.data.objects.new("Map " + map_name, None)
    bpy.context.collection.objects.link(map_empty)

    bpy.ops.object.select_all(action="DESELECT")
    map_empty.select_set(True)
    bpy.context.view_layer.objects.active = map_empty

    map_camera = bpy.data.cameras.new(name=f"Camera ({map_name})")
    map_camera.type = "ORTHO"
    map_camera.ortho_scale = 40
    map_camera = bpy.data.objects.new(f"Camera ({map_name})", map_camera)
    bpy.context.collection.objects.link(map_camera)
    map_camera.parent = map_empty
    map_camera.location.z += 20

    group_depth = 1
    for g_index, group in reversed(list(enumerate(map_groups))):
        group_name = f"Group {g_index}"
        if group.name:
            group_name += " " + group.name
        group_empty = bpy.data.objects.new(group_name, None)
        bpy.context.collection.objects.link(group_empty)
        group_empty.parent = map_empty
        group_empty.matrix_parent_inverse.identity()
        group_empty.location.z -= group_depth
        group_empty.location.x -= group.offset_x
        group_empty.location.y += group.offset_y

        parallax_constraint = group_empty.constraints.new("TRANSFORM")
        parallax_constraint.name = "Parallax"
        parallax_constraint.from_max_x = 100
        parallax_constraint.from_max_y = 100
        parallax_constraint.to_max_x = -group.parallax_x + 100
        parallax_constraint.to_max_y = -group.parallax_y + 100
        parallax_constraint.use_motion_extrapolate = True
        parallax_constraint.owner_space = "LOCAL"
        parallax_constraint.target_space = "LOCAL"
        parallax_constraint.target = map_camera

        layer_depth = 0
        for l_index, layer in reversed(list(enumerate(group.layers))):
            pos_identifier = f"{g_index}-{l_index}"
            if layer.kind() == "Tiles":
                layer_object = import_tiles_layer(layer, pos_identifier, materials, envelopes)
            elif layer.kind() == "Quads":
                layer_object = import_quads_layer(layer, pos_identifier, materials, envelopes)
            else:
                continue
            bpy.context.collection.objects.link(layer_object)
            layer_object.parent = group_empty
            layer_object.matrix_parent_inverse.identity()
            layer_object.location.z -= layer_depth
            layer_depth += 1
        group_depth += layer_depth + 1


def import_quads_layer(layer, pos_identifier, materials, envelopes):
    name = f"Quads {pos_identifier}"
    if layer.name:
        name = layer.name + " " + name

    layer_empty = bpy.data.objects.new(name, None)

    quad_amount = len(layer.quads)
    for i, quad in enumerate(layer.quads):
        position = quad.position
        vertices = quad.corners
        vertices[2], vertices[3] = vertices[3], vertices[2]
        vertices = [(vertex[0] - position[0], position[1] - vertex[1], 0) for vertex in vertices]

        quad_name = f"{name} {i}"
        quad_mesh = bpy.data.meshes.new(quad_name)
        quad_mesh.from_pydata(vertices, [], [range(4)])
        quad_mesh.update()

        uv_mapping = quad.texture_coords
        uv_mapping[2], uv_mapping[3] = uv_mapping[3], uv_mapping[2]
        uv_mapping = [(uv[0], 1 - uv[1]) for uv in uv_mapping]

        uv_map = quad_mesh.uv_layers.new(name="TwMap")
        for j in range(4):
            uv_map.data[j].uv = uv_mapping[j]

        vertex_colors = quad.colors
        vertex_colors[2], vertex_colors[3] = vertex_colors[3], vertex_colors[2]
        vertex_colors = [[v / 255 for v in color] for color in vertex_colors]
        vertex_coloring = quad_mesh.vertex_colors.new()
        for j in range(4):
            vertex_coloring.data[j].color = vertex_colors[j]

        quad_object = bpy.data.objects.new(quad_name, quad_mesh)
        bpy.context.collection.objects.link(quad_object)
        quad_object.parent = layer_empty
        quad_object.matrix_parent_inverse.identity()
        quad_object.location = (quad.position[0], -quad.position[1], -1 + (i + 1) / (quad_amount + 1))
        quad_object.visible_shadow = False
        if quad.position_env is not None:
            if envelopes.is_constant[quad.position_env]:
                env_x, env_y, env_rotation = envelopes.actions[quad.position_env]
                quad_object.location.x += env_x
                quad_object.location.y += env_y
                quad_object.rotation_euler.z += env_rotation
            else:
                envelopes.apply_action(quad_object, quad.position_env, quad.position_env_offset, "Position")

        if quad.color_env is not None:
            if envelopes.is_constant[quad.color_env]:
                r, g, b, a = envelopes.actions[quad.color_env]
                quad_object.color = [r, g, b, 1.0]
                quad_object.pass_index = a
            else:
                envelopes.apply_action(quad_object, quad.color_env, quad.color_env_offset, "Color")
        else:
            quad_object.pass_index = 1024
        quad_mesh.materials.append(materials.quad_material(layer.image))
    return layer_empty


def import_tiles_layer(layer, pos_identifier, materials, envelopes):
    name = f"Layer {pos_identifier}"
    if layer.name:
        name = layer.name + " " + name
    verts, faces, faces_uvs = layer.to_mesh()
    # create mesh
    layer_mesh = bpy.data.meshes.new(name)
    layer_mesh.from_pydata([vert + (0,) for vert in verts], [], faces)
    layer_mesh.update()

    # create object
    layer_object = bpy.data.objects.new(name, layer_mesh)
    layer_object.visible_shadow = False

    # assign material
    layer_object.data.materials.append(materials.tiles_layer_material(layer.color, layer.image))

    # assign uv map
    mesh_data = layer_object.data
    new_uv = mesh_data.uv_layers.new(name="UV " + name)
    for p, polygon in enumerate(layer_object.data.polygons):
        uvs = faces_uvs[p]
        for l, loop_index in enumerate(polygon.loop_indices):
            mesh_uv_loop = new_uv.data[loop_index]
            mesh_uv_loop.uv = uvs[l]

    if layer.color_env is not None:
        if envelopes.is_constant[layer.color_env]:
            r, g, b, a = envelopes.actions[layer.color_env]
            layer_object.color = [r, g, b, 1.0]
            layer_object.pass_index = a
        else:
            envelopes.apply_action(layer_object, layer.color_env, layer.color_env_offset, "Color")
    else:
        layer_object.pass_index = 1024

    return layer_object
